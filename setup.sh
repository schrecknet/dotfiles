#!/bin/bash
# A quick and dirty script to setup my env. Warning, there's no sanity checks
# on this thing at all, run at your own caution.
# 
# Requires git, curl and rsync.

# Install slimzsh
git clone --recursive https://github.com/changs/slimzsh.git ~/.slimzsh

# Install jellybeans colorscheme for vim
mkdir -p ~/.vim/colors
cd ~/.vim/colors
curl -O https://raw.githubusercontent.com/nanotech/jellybeans.vim/master/colors/jellybeans.vim

# Copy my stuff (last warning: overwrites everything without asking)
cd ~
git clone --recursive https://gitlab.com/schrecknet/dotfiles
rsync -avrI ~/dotfiles/ ~/ --exclude .git --exclude README.md --exclude setup.sh
