" Julia's vim config
" requires the 'jellybeans' colorscheme and 'Source Code Pro' font (for gvim)

set encoding=utf-8          " default encoding to UTF-8
set nocompatible            " vim > vi
set ttyfast                 " indicate fast connection, disable for slower ones
let mapleader="\<Space>"    " Leader = Spacebar


" INTERFACE-RELATED SETTINGS
colorscheme jellybeans      " set colorscheme
set colorcolumn=80          " colorize the 80th column
syntax on                   " enable syntax
set ruler                   " show current line number at bottom right
set number                  " enable line numbers
set relativenumber          " enable relative line numbers
set cursorline              " highlight the row where the cursor is
set showcmd                 " show partial command at the last screen line
set mouse=a                 " enable mouse for all modes
set scrolloff=5             " number of lines to keep visible b/a current line
set history=500             " store a history of 500 : commands
set belloff=all             " disable the errorbell sound
set display=truncate        " show '@@@' in the last line if it's truncated

" platform-specific GUI-settings, like font & window size
if has("gui_running")
  if has("gui_gtk2")
    set guifont=Source\ Code\ Pro
    set guioptions=
    set lines=40 columns=86
  elseif has("gui_macvim")
    set guifont=enlo\ Regular:h14
    set guioptions=
    set lines=40 columns=86
  elseif has("gui_win32")
    "set guifont=Consolas:h11:cANSI
    set guifont=Source\ Code\ Pro:h11:cANSI
    set guioptions=
    set lines=40 columns=86
  endif
endif


" TAB/INDENATION-RELATED SETTINGS
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
set autoindent              " minimal automatic indentation for any filetype
filetype plugin indent on   " enables a lot of filetype-specific useful stuff


" KEYBINDING-RELATED SETTINGS
set backspace=indent,eol,start  " sane backspace behaviour
nnoremap / /\v
vnoremap / /\v
nnoremap \l :setlocal relativenumber!<CR>
nnoremap <leader>l :setlocal relativenumber!<CR>

" moving lines around easily (leader + j/k to move up/down):
nnoremap <leader>k :m-2<CR>==
nnoremap <leader>j :m+<CR>==
xnoremap <leader>k :m-2<CR>gv=gv
xnoremap <leader>j :m'>+<CR>gv=gv

" F11 toggles the menubar on gvim
nnoremap <F11> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>


" OTHER/MISC SETTINGS
set modelines=5             " check 5 lines at start/end for vim cmds in file

" searching:
set smartcase   " ignores case on search if everything is lowercase
set incsearch   " enable incremental search
set hlsearch    " auto-highlight search, use :nohlsearch to disable
set wildmenu    " command-line completion with TAB and CR
"set showmatch   " cursor will briefly jump to matching parentheses

" undo, history, backups etc.
set backup      " enable backups
set undofile    " enable undo file
set swapfile    " enable swap file

" store backups
set backupdir=/tmp//,$TMPDIR//,$TEMP//,$TMP//,.
set undodir=/tmp//,$TMPDIR//,$TEMP//,$TMP//,.

" if running windows, store backups in C:\temp
if has("win32") || has("win64") 
    set backupdir=C:\\temp//,.
    set undodir=C:\\temp//,.
endif

" markdown in win10 workflow stuff
au FileType markdown setlocal foldlevel=99
"set nofoldenable
let g:markdown_folding = 1
" reminder: make sure mdview.exe is in PATH
command Md :!start /B mdview.exe %:S
