# Julia's zsh config.
# Requires slimzsh: https://github.com/changs/slimzsh

# init slimzsh
source "$HOME/.slimzsh/slim.zsh"
# define X server display for win10 env
export DISPLAY=:0
# auto-run xrdb
xrdb $HOME/.Xresources
# define editors
export EDITOR="vim"
export VISUAL="vim"


# nnn stuff (https://github.com/jarun/nnn)
export NNN_OPTS="acdEHior"
# wsl opener, needs the latest wslu, there's also a ~4s delay on it, but it
# works. Make sure 'c' is set above as well or it might not work.
# TODO: make this system agnostic so it works on non-wsl setups
export NNN_OPENER="wslview"
# Quit cd command setup, copypaste from 'nnn/misc/quitcd/quitcd.bash_sh_zsh'
n ()
{
    # Block nesting of nnn in subshells
    [ "${NNNLVL:-0}" -eq 0 ] || {
        echo "nnn is already running"
        return
    }

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # If NNN_TMPFILE is set to a custom path, it must be exported for nnn to
    # see. To cd on quit only on ^G, remove the "export" and make sure not to
    # use a custom path, i.e. set NNN_TMPFILE *exactly* as follows:
    #      NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    # The command builtin allows one to alias nnn to n, if desired, without
    # making an infinitely recursive alias
    command nnn "$@"

    [ ! -f "$NNN_TMPFILE" ] || {
        . "$NNN_TMPFILE"
        rm -f "$NNN_TMPFILE" > /dev/null
    }
}


# slimzsh stuff
prompt_newline='%666v'
PURE_PROMPT_SYMBOL=" »"
#PURE_PROMPT_SYMBOL=" %F{087}j%F{219}u%F{231}l%F{219}i%F{087}a%F{155} »%F{default}"
PURE_PROMPT_VICMD_SYMBOL="<"
PURE_GIT_UP_ARROW="↑"
PURE_GIT_DOWN_ARROW="↓"


# Color for manpages in less, makes manpages a little easier to read
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'
