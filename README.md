# Dotfiles
My various dotfiles. I mainly run Win10/WSL nowadays on my desktop but all the
files should obviously work fine on pure Linux, there's just some
Windows-specific configuration such as exporting DISPLAY=:0 (for Xming) and
calling various Windows binaries.

I use the excellent [slimzsh by changs](https://github.com/changs/slimzsh) as a
base for my zsh config.

For vim, I currently have
[jellybeans.vim](https://github.com/nanotech/jellybeans.vim) as my default
theme, supported by the beautiful [Source Code
Pro](https://fonts.google.com/specimen/Source+Code+Pro) font (which I use
pretty much everywhere on my system as my monospace font of choice).

## Other workflow stuff
I do a lot of note taking in markdown files synced to a cloud. My general
workflow is to use vim/gvim on Windows 10, and occasionally use [this
standalone markdown viewer by c3er](https://github.com/c3er/mdview) to
view and navigate files with a lot of pictures, links, etc., more easily.

Recently, I've also grown quite fond of the [n<sup>3</sup> file
manager](https://github.com/jarun/nnn) for doing some quick navigation and file
shuffling faster than the usual CLI shell dance, though I'm also accustomed to
using vim's built-in `netrw` as well.
